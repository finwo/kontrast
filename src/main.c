#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>

#include "main.h"

int argsc = 0;
char **argsv;

int strpos( char *haystack, char *needle ) {
  char *p = strstr( haystack, needle );
  if(p) return p - haystack;
  return -1;
}

int main( int argc, char **argv ) {
  argsc = argc;
  argsv = argv;
  return 0;
}

#ifdef __cplusplus
} // extern "C"
#endif
