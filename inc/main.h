#ifdef __cplusplus
extern "C" {
#endif

#define INIT __attribute__ ((__constructor__)) void
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

int strpos( char *haystack, char *needle );

#ifdef __cplusplus
} // extern "C"
#endif
