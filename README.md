Finwo / Kontrast
================

This is a git-push deployment system based upon chroot images

## Installation

`todo`

## Usage

`todo`

## Contributing

After checking the [Github issues](http://github.com/finwo/kontrast/issues) and confirming that your request isn't already being worked on, feel free to spawn a new fork of the `develop` branch & send in a pull request. Keep in mind to describe what your intent is and why you want the feature you've just build merged into the main repository.

The `develop` branch is merged into the `master` periodically after confirming it's stable, to ensure the master always contains a production-ready version.

## License

MIT License

Copyright (c) 2016 Finwo

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
